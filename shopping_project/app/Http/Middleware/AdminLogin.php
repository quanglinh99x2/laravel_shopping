<?php

namespace App\Http\Middleware;

use App\Role;
use App\User;
use Closure;
use Illuminate\Support\Facades\Auth;

class AdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // return $next($request);
        if(Auth::check()){
            foreach(Auth()->user()->roles  as $item){
                if($item->name == "guest"){
                    // return redirect('admin');
                    Auth::logout();
                    return redirect('admin');
                }
            }
            return $next($request);
        }
        else{
            return redirect('admin');
        }
    }
}
