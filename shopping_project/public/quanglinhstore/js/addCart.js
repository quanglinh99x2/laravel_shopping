
 $(document).ready(function () {
    $(".mega-menu-category").css("display", "none");
    // Thêm vào cart
    $('.add-to-cart-mt').click(function (e) {
        e.preventDefault();
        var id_product = $(this).attr('data-id');
        var url = $(this).attr('url-temp');
        // alert(url);
        $.ajax({
            type: "post",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content'), '_method': 'patch'},
            url: url,
            data: {
                'id_product':id_product,
            },
            success: function (data) {
                // alert("oke");
                // console.log(JSON.parse(data));
                // $(".mini-products-list").empty();
                // $(".mini-products-list").html(data);
                $(".top-cart-content").empty();
                $(".top-cart-content").html(data);
                alertify.success('Thêm vào giỏ hàng thành công !');
            }

        });
    });

    //xóa dữ liệu cart header
    $(document).on("click",".remove-cart",function (e) {
        e.preventDefault();
        var pro_id_delete = $(this).attr('id-pro');
        var url = $(this).attr('url-temp');
        // alert(pro_id_delete);
        $.ajax({
            type: "post",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content'), '_method': 'patch'},
            url: url,
            data: {
                'pro_id_delete':pro_id_delete,
            },
            success: function (data) {
                $(".top-cart-content").empty();
                $(".top-cart-content").html(data);
                alertify.success('Xóa sản phẩm khỏi giỏ hàng thành công !');
            }
        });
    });
  });

