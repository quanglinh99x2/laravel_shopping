<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from htmlmystore.justthemevalley.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 12 Dec 2020 11:41:56 GMT -->
<head>
<!-- Basic page needs -->
<meta charset="utf-8">
<!--[if IE]>
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <![endif]-->
<meta http-equiv="x-ua-compatible" content="ie=edge">

@yield('title')

<meta name="description" content="best template, template free, responsive theme, fashion store, responsive theme, responsive html theme, Premium website templates, web templates, Multi-Purpose Responsive HTML5 Template">
<meta name="keywords" content="bootstrap, ecommerce, fashion, layout, responsive, responsive template, responsive template download, responsive theme, retail, shop, shopping, store, Premium website templates, web templates, Multi-Purpose Responsive HTML5 Template"/>
<!-- Mobile specific metas  , -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}" />
<!-- Favicon  -->
<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">

<!-- Google Fonts -->
<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700italic,700,400italic' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Dosis:400,300,200,500,600,700,800' rel='stylesheet' type='text/css'>

<!-- CSS Style -->

<!-- Bootstrap CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/bootstrap.min.css') }}">

<!-- font-awesome & simple line icons CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/font-awesome.css') }}" media="all">
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/simple-line-icons.css') }}" media="all">

<!-- owl.carousel CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/owl.theme.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/owl.transitions.css') }}">

<!-- animate CSS  -->
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/animate.css') }}" media="all">

<!-- flexslider CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/flexslider.css') }}" >

<!-- jquery-ui.min CSS  -->
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/jquery-ui.css') }}">

<!-- Revolution Slider CSS -->
<link href="{{ asset('quanglinhstore/css/revolution-slider.css') }}" rel="stylesheet">

<!-- style CSS -->
<link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/style.css') }}" media="all">

<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/alertify.min.js"></script>

<!-- CSS -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.min.css"/>

<!--
    RTL version
-->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/alertify.rtl.min.css"/>
<!-- Default theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/default.rtl.min.css"/>
<!-- Semantic UI theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/semantic.rtl.min.css"/>
<!-- Bootstrap theme -->
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.13.1/build/css/themes/bootstrap.rtl.min.css"/>

@yield('css')
</head>

<body class="cms-index-index cms-home-page">

<!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

<!-- mobile menu -->
<div id="mobile-menu">
  <ul>
    <li><a href="index.html" class="home1">Home</a>
      <ul>
        <li><a href="index.html"><span>Home Version 1</span></a></li>
        <li><a href="Version2/index.html"><span>Home Version 2</span></a></li>
        <li><a href="Version3/index.html"><span>Home Version 3</span></a></li>
        <li><a href="Version4/index.html"><span>Home Version 4</span></a></li>
        <li><a href="Version5/index.html"><span>Home Version 5</span></a></li>
        <li><a href="rtl-version/index.html"><span>RTL Version</span></a></li>
        <li><a href="rtl-version1/index.html"><span>Home Version 1 RTL</span></a></li>
        </ul>
    </li>
    <li><a href="shop_grid.html">Pages</a>
      <ul>
        <li><a href="#" class="">Shop Pages </a>
          <ul>
            <li> <a href="shop_grid.html"> Shop grid </a> </li>
            <li> <a href="shop_grid_right_sidebar.html"> Shop grid right sidebar</a> </li>
            <li> <a href="shop_list.html"> Shop list </a> </li>
            <li> <a href="shop_list_right_sidebar.html"> Shop list right sidebar</a> </li>
            <li> <a href="shop_grid_full_width.html"> Shop Full width </a> </li>
          </ul>
        </li>
        <li><a href="#">Ecommerce Pages </a>
          <ul>
            <li> <a href="wishlist.html"> Wishlists </a> </li>
            <li> <a href="checkout.html"> Checkout </a> </li>
            <li> <a href="compare.html"> Compare </a> </li>
            <li> <a href="shopping_cart.html"> Shopping cart </a> </li>
            <li> <a href="quick_view.html"> Quick View </a> </li>
          </ul>
        </li>
        <li> <a href="#">Static Pages </a>
          <ul>
            <li> <a href="account_page.html"> Create Account Page </a> </li>
            <li> <a href="about_us.html"> About Us </a> </li>
            <li> <a href="contact_us.html"> Contact us </a> </li>
            <li> <a href="404error.html"> Error 404 </a> </li>
            <li> <a href="faq.html"> FAQ </a> </li>
          </ul>
        </li>
        <li> <a href="#">Product Categories </a>
          <ul>
            <li> <a href="cat-3-col.html"> 3 Column Sidebar </a> </li>
            <li> <a href="cat-4-col.html"> 4 Column Sidebar</a> </li>
            <li> <a href="cat-4-col-full.html"> 4 Column Full width </a> </li>
            <li> <a href="cat-6-col.html"> 6 Columns Full width</a> </li>
          </ul>
        </li>
        <li> <a href="#">Single Product Pages </a>
          <ul>
            <li><a href="single_product.html"> single product </a> </li>
            <li> <a href="single_product_left_sidebar.html"> single product left sidebar</a> </li>
            <li> <a href="single_product_right_sidebar.html"> single product right sidebar</a> </li>
            <li> <a href="single_product_magnify_zoom.html"> single product magnify zoom</a> </li>
          </ul>
        </li>
        <li> <a href="#"> Blog Pages </a>
          <ul>
            <li><a href="blog_right_sidebar.html">Blog – Right sidebar </a></li>
            <li><a href="blog_left_sidebar.html">Blog – Left sidebar </a></li>
            <li><a href="blog_full_width.html">Blog_ - Full width</a></li>
            <li><a href="single_post.html">Single post </a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="contact_us.html">Contact us</a></li>
    <li><a href="about_us.html">About us</a></li>
    <li><a href="blog.html">Blog</a>
      <ul>
        <li><a href="blog_right_sidebar.html">Blog – Right sidebar </a></li>
        <li><a href="blog_left_sidebar.html">Blog – Left sidebar </a></li>
        <li><a href="blog_full_width.html">Blog_ - Full width</a></li>
        <li><a href="single_post.html">Single post </a></li>
      </ul>
    </li>
    <li><a href="shop_grid.html">Camera & Photo</a>
      <ul>
        <li><a href="#" class="">Accessories</a>
          <ul>
            <li><a href="shop_grid.html">Cocktail</a></li>
            <li><a href="shop_grid.html">Day</a></li>
            <li><a href="shop_grid.html">Evening</a></li>
            <li><a href="shop_grid.html">Sundresses</a></li>
          </ul>
        </li>
        <li><a href="#">Dresses</a>
          <ul>
            <li><a href="shop_grid.html">Accessories</a></li>
            <li><a href="shop_grid.html">Hats and Gloves</a></li>
            <li><a href="shop_grid.html">Lifestyle</a></li>
            <li><a href="shop_grid.html">Bras</a></li>
          </ul>
        </li>
        <li> <a href="#">Shoes</a>
          <ul>
            <li> <a href="shop_grid.html">Flat Shoes</a> </li>
            <li> <a href="shop_grid.html">Flat Sandals</a> </li>
            <li> <a href="shop_grid.html">Boots</a> </li>
            <li> <a href="shop_grid.html">Heels</a> </li>
          </ul>
        </li>
        <li> <a href="#">Jwellery</a>
          <ul>
            <li> <a href="shop_grid.html">Bracelets</a> </li>
            <li> <a href="shop_grid.html">Necklaces &amp; Pendent</a> </li>
            <li> <a href="shop_grid.html">Pendants</a> </li>
            <li> <a href="shop_grid.html">Pins &amp; Brooches</a> </li>
          </ul>
        </li>
        <li> <a href="#">Dresses</a>
          <ul>
            <li> <a href="shop_grid.html">Casual Dresses</a> </li>
            <li> <a href="shop_grid.html">Evening</a> </li>
            <li> <a href="shop_grid.html">Designer</a> </li>
            <li> <a href="shop_grid.html">Party</a> </li>
          </ul>
        </li>
        <li> <a href="#">Swimwear</a>
          <ul>
            <li> <a href="shop_grid.html">Swimsuits</a> </li>
            <li> <a href="shop_grid.html">Beach Clothing</a> </li>
            <li> <a href="shop_grid.html">Clothing</a> </li>
            <li> <a href="shop_grid.html">Bikinis</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="shop_grid.html">Computers</a>
      <ul>
        <li> <a href="#" class="">Clothing</a>
          <ul class="level1">
            <li class="level2"><a href="shop_grid.html">Coats &amp; Jackets</a> </li>
            <li class="level2"><a href="shop_grid.html">Raincoats</a> </li>
            <li class="level2"><a href="shop_grid.html">Blazers</a> </li>
            <li class="level2"><a href="shop_grid.html">Jackets</a> </li>
          </ul>
        </li>
        <li> <a href="#">Dresses</a>
          <ul class="level1">
            <li class="level2"><a href="shop_grid.html">Casual Dresses</a> </li>
            <li class="level2"><a href="shop_grid.html">Evening</a> </li>
            <li class="level2"><a href="shop_grid.html">Designer</a> </li>
            <li class="level2"><a href="shop_grid.html">Party</a> </li>
          </ul>
        </li>
        <li> <a href="#" class="">Shoes</a>
          <ul class="level1">
            <li class="level2"><a href="shop_grid.html">Sport Shoes</a> </li>
            <li class="level2"><a href="shop_grid.html">Casual Shoes</a> </li>
            <li class="level2"><a href="shop_grid.html">Leather Shoes</a> </li>
            <li class="level2"><a href="shop_grid.html">canvas shoes</a> </li>
          </ul>
        </li>
        <li> <a href="#">Jackets</a>
          <ul class="level1">
            <li class="level2"><a href="shop_grid.html">Coats</a> </li>
            <li class="level2"><a href="shop_grid.html">Formal Jackets</a> </li>
            <li class="level2"><a href="shop_grid.html">Leather Jackets</a> </li>
            <li class="level2"><a href="shop_grid.html">Blazers</a> </li>
          </ul>
        </li>
        <li> <a href="#">Accesories</a>
          <ul class="level1">
            <li class="level2"><a href="shop_grid.html">Backpacks</a> </li>
            <li class="level2"><a href="shop_grid.html">Wallets</a> </li>
            <li class="level2"><a href="shop_grid.html">Laptops Bags</a> </li>
            <li class="level2"><a href="shop_grid.html">Belts</a> </li>
          </ul>
        </li>
      </ul>
    </li>
    <li><a href="shop_grid.html">Apple Store</a>
      <ul>
        <li> <a href="shop_grid.html">Mobiles</a>
          <ul>
            <li> <a href="shop_grid.html">iPhone</a> </li>
            <li> <a href="shop_grid.html">Samsung</a> </li>
            <li> <a href="shop_grid.html">Nokia</a> </li>
            <li> <a href="shop_grid.html">Sony</a> </li>
          </ul>
        </li>
        <li> <a href="shop_grid.html" class="">Music &amp; Audio</a>
          <ul>
            <li> <a href="shop_grid.html">Audio</a> </li>
            <li> <a href="shop_grid.html">Cameras</a> </li>
            <li> <a href="shop_grid.html">Appling</a> </li>
            <li> <a href="shop_grid.html">Car Music</a> </li>
          </ul>
        </li>
        <li> <a href="shop_grid.html">Accessories</a>
          <ul>
            <li> <a href="shop_grid.html">Home &amp; Office</a> </li>
            <li> <a href="shop_grid.html">TV &amp; Home Theater</a> </li>
            <li> <a href="shop_grid.html">Phones &amp; Radio</a> </li>
            <li> <a href="shop_grid.html">All Electronics</a> </li>
          </ul>
        </li>
      </ul>
    </li>
  </ul>
</div>
<!-- end mobile menu -->
<div id="page">

@include('partials.header');



  <!--Newsletter Popup Start-->
@yield('content')
 <!--End of Newsletter Popup-->
  </div>

@include('partials.footer')
<!-- JS -->

<!-- jquery js -->
<script src="{{ asset('quanglinhstore/js/jquery.min.js') }}"></script>

<!-- bootstrap js -->
<script src="{{ asset('quanglinhstore/js/bootstrap.min.js') }}"></script>


<!-- owl.carousel.min js -->
<script src="{{ asset('quanglinhstore/js/owl.carousel.min.js') }}"></script>

<!-- bxslider js -->
<script src="{{ asset('quanglinhstore/js/jquery.bxslider.js') }}"></script>

<!-- Slider Js -->
<script src="{{ asset('quanglinhstore/js/revolution-slider.js') }}"></script>

<!-- megamenu js -->
<script src="{{ asset('quanglinhstore/js/megamenu.js') }}"></script>
<script>
  /* <![CDATA[ */
  var mega_menu = '0';

  /* ]]> */
  </script>

<!-- jquery.mobile-menu js -->
<script src="{{ asset('quanglinhstore/js/mobile-menu.js') }}"></script>

<!--jquery-ui.min js -->
<script src="{{ asset('quanglinhstore/js/jquery-ui.js') }}"></script>

<!-- main js -->
<script src="{{ asset('quanglinhstore/js/main.js') }}"></script>

<!-- countdown js -->
<script src="{{ asset('quanglinhstore/js/countdown.js') }}"></script>

<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- Revolution Slider -->
<script>
          jQuery(document).ready(function() {
                 jQuery('.tp-banner').revolution(
                  {
                      delay:9000,
                      startwidth:1170,
                      startheight:530,
                      hideThumbs:10,

                      navigationType:"bullet",
                      navigationStyle:"preview1",

                      hideArrowsOnMobile:"on",

                      touchenabled:"on",
                      onHoverStop:"on",
                      spinner:"spinner4"
                  });

          });
</script>

<!-- Hot Deals Timer 1-->
<script>
      var dthen1 = new Date("12/25/20 11:59:00 PM");
      start = "06/04/18 03:02:11 AM";
      start_date = Date.parse(start);
      var dnow1 = new Date(start_date);
      if(CountStepper>0)
          ddiff= new Date((dnow1)-(dthen1));
      else
          ddiff = new Date((dthen1)-(dnow1));
      gsecs1 = Math.floor(ddiff.valueOf()/1000);

      var iid1 = "countbox_1";
      CountBack_slider(gsecs1,"countbox_1", 1);
  </script>

@yield('js')
</body>

<!-- Mirrored from htmlmystore.justthemevalley.com/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 12 Dec 2020 11:45:12 GMT -->
</html>
