@extends('layouts.index')
@section('title')
<title>Giỏ hàng</title>
@endsection
@section('css')
{{-- <link rel="stylesheet" type="text/css" href="{{ asset('quanglinhstore/css/flexslider.css') }}" > --}}
@endsection
@section('content')
     <!-- Main Container -->
  <section class="main-container col1-layout">
    <div class="main container">
      <div class="col-main">
        <div class="cart">

          <div class="page-content page-order"><div class="page-title">
            <h2>Shopping Cart</h2>
          </div>


            <div class="order-detail-content">
              <div class="table-responsive">
                <table class="table table-bordered cart_summary">
                    @include('pages.list_product_cart')
                </table>
              </div>
              <div class="cart_navigation"> <a class="continue-btn" href="#"><i class="fa fa-arrow-left"> </i>&nbsp; Tiếp tục mua hàng</a><a class="continue-btn" href="{{ route('cart.destroy') }}"><i class="fa fa-trash bg-danger"> </i>&nbsp; Xóa toàn bộ giỏ hàng</a> <a class="checkout-btn" href="#"><i class="fa fa-check"></i> Thanh toán</a> </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('js')
<script src="{{ asset('quanglinhstore/js/addCart.js') }}"></script>
<script>
    $(document).ready(function () {
    $(".mega-menu-category").css("display", "none");
    // Thêm vào cart
    $(document).on("click",".remove_item",function (e) {
        e.preventDefault();
        var pro_id_delete = $(this).attr('id-pro');
        var url = $(this).attr('url-temp');
        // alert(pro_id_delete);
        $.ajax({
            type: "post",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content'), '_method': 'patch'},
            url: url,
            data: {
                'pro_id_delete':pro_id_delete,
            },
            success: function (data) {
                $(".table-bordered").empty();
                $(".table-bordered").html(data);

                alertify.success('Xóa sản phẩm khỏi giỏ hàng thành công !');
            }
        });
    });
    $(document).on("change",".input-sm",function (e) {
        e.preventDefault();
        var pro_id = $(this).attr('id-pro');
        var url = $(this).attr('url-temp');
        var qty_item = $(this).val();

        $.ajax({
            type: "post",
            headers: {'X-CSRF-TOKEN': $('meta[name="csrf_token"]').attr('content'), '_method': 'patch'},
            url: url,
            data: {
                'pro_id':pro_id,
                'pro_qty':qty_item
            },
            success: function (data) {
                $(".table-bordered").empty();
                $(".table-bordered").html(data);

                alertify.success('Cập nhật giỏ hàng thành công !');
            }
        });
    });
});
</script>
@endsection
