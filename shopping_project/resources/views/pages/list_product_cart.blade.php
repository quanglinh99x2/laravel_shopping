<thead>
    <tr>
      <th class="cart_product">Ảnh sản phẩm</th>
      <th>Tên sản phẩm</th>
      <th>Đơn giá</th>
      <th>Số lượng</th>
      <th>Thành tiền</th>
      <th  class="action"><i class="fa fa-trash-o"></i></th>
    </tr>
  </thead>
  <tbody>
    @if (!empty(request()->session()->get('cart')))
        @foreach (request()->session()->get('cart')->products as $item)
        <tr>
            <td class="cart_product"><a href="#"><img src="{{ asset($item['productInfo']['product_image_path']) }}" alt="Product"></a></td>
            <td class="cart_description"><p class="product-name"><a href="#">{{ $item['productInfo']['name'] }} </a></p>
              <small><a href="#">Color : Red</a></small><br>
              <small><a href="#">Size : M</a></small></td>

            <td class="price"><span>{{ number_format($item['productInfo']['price'],0,'',',') }} đ</span></td>
            <td class="qty"><input url-temp="{{ route('cart.update_product') }}" id-pro="{{  $item['productInfo']['id'] }}"  class="form-control input-sm" min=1 max = 10 type="number" value="{{ $item['quantity'] }}"></td>
            <td class="price"><span>{{ number_format($item['price'],0,'',',') }} đ</span></td>
            <td class="action"><a url-temp="{{ route('cart.delete_item_on') }}" id-pro="{{  $item['productInfo']['id'] }}" class="remove_item" href="#"><i class="icon-close"></i></a></td>
          </tr>
        @endforeach
    @endif

  </tbody>
  <tfoot>
    <tr>
      <td colspan="2" rowspan="2"></td>
      {{-- <td colspan="3">Total products (tax incl.)</td> --}}
      {{-- <td colspan="2">{{ number_format(request()->session()->get('cart')->totalPrice,0,'',',') }} đ</td> --}}
    </tr>
    <tr>
      <td colspan="3"><strong>Total</strong></td>
      <td colspan="2"><strong>@if (!empty(request()->session()->get('cart')))
        {{ number_format(request()->session()->get('cart')->totalPrice,0,'',',') }}
      @endif đ</strong></td>
    </tr>
  </tfoot>
