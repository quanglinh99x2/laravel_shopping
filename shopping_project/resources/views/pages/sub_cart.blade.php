@if (!empty($newCart))
    <div class="block-subtitle hidden-xs">Recently added item(s)</div>
    <ul id="cart-sidebar" class="mini-products-list">
    @foreach ($newCart->products as $cartItem)
        <li class="item odd"> <a href="shopping_cart.html" title="{{ $cartItem['productInfo']['name'] }}" class="product-image"><img src="{{ asset($cartItem['productInfo']['product_image_path']) }}" alt="html template" width="65"></a>
            <div class="product-details"> <span href="#" id-pro ="{{ $cartItem['productInfo']['id'] }}" url-temp="{{ route('cart.delete_item') }}"  title="Remove This Item" class="remove-cart"><i class="icon-close"></i></span>
            <p class="product-name"><a href="shopping_cart.html">{{ $cartItem['productInfo']['name'] }}</a> </p>
            <strong>{{ $cartItem['quantity'] }}</strong> x <span class="price">{{ number_format($cartItem['productInfo']['price'],0,'',',') }} đ</span> </div>
        </li>
    @endforeach
    </ul>
    <div class="actions">
        <div class="top-subtotal">Subtotal: <span class="price-total-cart"> @if(!empty(request()->session()->get('cart'))) {{ number_format(request()->session()->get('cart')->totalPrice,0,'',',') }} đ @endif</span></div>
      <button class="btn-checkout" type="button"><i class="fa fa-check"></i><span>Checkout</span></button>
      <button class="view-cart" type="button"><i class="fa fa-shopping-cart"></i> <a href="{{ route('cart.index') }}"><span>View Cart</span></a></button>
    </div>
@else
    <div class="block-subtitle hidden-xs">Recently added item(s)</div>
    <ul id="cart-sidebar" class="mini-products-list">

    </ul>
    <div class="actions">
        <div class="top-subtotal">Subtotal: <span class="price-total-cart"> 0 đ </span></div>
      <button class="btn-checkout" type="button"><i class="fa fa-check"></i><span>Checkout</span></button>
      <button class="view-cart" type="button"><i class="fa fa-shopping-cart"></i> <a href="{{ route('cart.index') }}"><span>View Cart</span></a></button>
    </div>

@endif
